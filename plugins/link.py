from pyrogram import Client, Filters, Message

from database import Chat


def url_filter():
    return Filters.create(
        lambda _, m: bool(m.entities and len(list(filter(lambda entity: entity['type'] == 'url', m.entities))) > 0),
        "URLFilter")


@Client.on_message(url_filter() & Filters.group, group=1)
def my_link_handler(client: Client, message: Message):
    print("link was detected")
    group = Chat.get_without_failing((Chat.id == message.chat.id))
    if group is not None:
        if group.ban_link:
            member = client.get_chat_member(chat_id=group.id, user_id=message.from_user.id)
            if member.status in ['creator', 'administrator']:
                pass
            else:
                try:
                    message.delete(revoke=True)
                except Exception as e:
                    print(e)
