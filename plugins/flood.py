import time

from pyrogram import Client, Filters, Message

import utils
from vars import Vars


@Client.on_message(Filters.group, group=0)
def flood_watcher(client: Client, m: Message):
    if f'group_{m.chat.id}' not in Vars.GROUP_ADMINS:
        Vars.GROUP_ADMINS[f'group_{m.chat.id}'] = utils.get_chat_admins(client, m)
    if m.from_user.id not in Vars.GROUP_ADMINS[f'group_{m.chat.id}']:
        if f'group_{m.chat.id}' in Vars.FLOOD_LAST_MESSAGE_USER:
            if f'user_{m.from_user.id}' in Vars.FLOOD_LAST_MESSAGE_USER[f'group_{m.chat.id}']:
                Vars.FLOOD_LAST_MESSAGE_USER[f'group_{m.chat.id}'][f'user_{m.from_user.id}'] += 1
                if Vars.FLOOD_LAST_MESSAGE_USER[f'group_{m.chat.id}'][f'user_{m.from_user.id}'] > 4:
                    client.restrict_chat_member(m.chat.id, m.from_user.id, int(time.time() + Vars.FLOOD_MUTE_TIME))
                    utils.warn(client, m, 'flood')
                else:
                    time.sleep(1.5)
                    try:
                        Vars.FLOOD_LAST_MESSAGE_USER[f'group_{m.chat.id}'][f'user_{m.from_user.id}'] -= 1
                        if Vars.FLOOD_LAST_MESSAGE_USER[f'group_{m.chat.id}'][f'user_{m.from_user.id}'] == 1:
                            del Vars.FLOOD_LAST_MESSAGE_USER[f'group_{m.chat.id}'][f'user_{m.from_user.id}']
                    except KeyError as e:
                        print(e)
            else:
                Vars.FLOOD_LAST_MESSAGE_USER[f'group_{m.chat.id}'][f'user_{m.from_user.id}'] = 1
        else:
            Vars.FLOOD_LAST_MESSAGE_USER[f'group_{m.chat.id}'] = {}
