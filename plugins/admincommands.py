from pyrogram import Client, Filters, Message, InlineKeyboardMarkup, InlineKeyboardButton
from database import Chat

not_admin_message = "Make me an admin so i can do the commands."


@Client.on_message(Filters.group &
                   Filters.command(
                       ["ban", "unban", "video", "video_note", "audio", "voice", "link", "photo", "sticker",
                        "file", "text", "bot",
                        "gif",
                        "mute", "kick", "unmute",
                        "auto", "lock", "all", "setautolock", "setautounlock", "clear"],
                       prefix=["!", "/"], separator=" "), group=1)
def my_commands_handler(client: Client, message: Message):
    print(message.command)
    group = Chat.get_without_failing((Chat.id == message.chat.id))
    print("command detected")
    me = client.get_chat_member(chat_id=message.chat.id, user_id="MyfoldersBot")
    if me.status != "administrator":
        message.reply("I don't have enough permissions to restrict users")

    if group is not None:
        member = client.get_chat_member(group.id, user_id=message.from_user.id)
        if member.status in ["creator", "administrator"]:
            if "ban" in message.command:
                if len(message.command) == 2:
                    if "video" in message.command:
                        group.ban_video = True
                        group.save()
                    elif "photo" in message.command:
                        group.ban_photo = True
                        group.save()
                    elif "voice" in message.command:
                        group.ban_voice = True
                        group.save()
                    elif "sticker" in message.command:
                        group.ban_sticker = True
                        group.save()
                    elif "gif" in message.command:
                        group.ban_gif = True
                        group.save()
                    elif "audio" in message.command:
                        group.ban_audio = True
                        group.save()
                    elif "link" in message.command:
                        group.ban_link = True
                        group.save()
                    elif "video_note" in message.command:
                        group.ban_video_note = True
                        group.save()
                    elif "file" in message.command:
                        group.ban_file = True
                        group.save()
                    elif "all" in message.command:
                        group.ban_photo = True
                        group.ban_link = True
                        group.ban_file = True
                        group.ban_gif = True
                        group.ban_sticker = True
                        group.ban_audio = True
                        group.ban_video_note = True
                        group.ban_video = True
                        group.ban_voice = True
                        client.restrict_chat(chat_id=message.chat.id, can_invite_users=True)
                        group.save()
                    message.reply("{} was banned for this group.".format(message.command[1]))
            elif "unban" in message.command:
                if len(message.command) == 2:
                    if message.text == "!unban voice":
                        group.ban_voice = False
                        group.save()

                    elif "video" in message.command:
                        group.ban_video = False
                        group.save()
                    elif "photo" in message.command:
                        group.ban_photo = False
                        group.save()
                    elif "video_note" in message.command:
                        group.ban_video_note = False
                        group.save()
                    elif "audio" in message.command:
                        group.ban_audio = False
                        group.save()
                    elif "sticker" in message.command:
                        group.ban_sticker = False
                        group.save()
                    elif "gif" in message.command:
                        group.ban_gif = False
                        group.save()
                    elif "file" in message.command:
                        group.ban_file = False
                        group.save()
                    elif "link" in message.command:
                        group.ban_link = False
                        group.save()

                    elif "all" in message.command:
                        print("!unban all")
                        group.ban_photo = False
                        group.ban_link = False
                        group.ban_file = False
                        group.ban_gif = False
                        group.ban_sticker = False
                        group.ban_audio = False
                        group.ban_video_note = False
                        group.ban_video = False
                        group.ban_voice = False
                        client.restrict_chat(chat_id=message.chat.id, can_send_messages=True,
                                             can_send_media_messages=True,
                                             can_send_other_messages=True, can_invite_users=True,
                                             can_add_web_page_previews=True,
                                             can_send_polls=True)
                        group.save()
                    message.reply("{} was unbanned for this group.".format(message.command[1]))
            elif message.text == "!mute":
                try:
                    if message.reply_to_message:
                        client.restrict_chat_member(chat_id=message.chat.id,
                                                    user_id=message.reply_to_message.from_user.id)
                        btn_unmute = InlineKeyboardMarkup([
                            [InlineKeyboardButton("Unmute", callback_data="unmute {}".format(
                                message.reply_to_message.from_user.id))]])
                        message.reply(
                            "user **{}** has been muted.".format(message.reply_to_message.from_user.first_name),
                            reply_markup=btn_unmute)
                    else:
                        message.reply("Please reply on a message of the user you wish to mute!")

                except Exception as e:
                    print(e)
                pass
            elif "unmute" in message.command:
                print("umute")
                client.restrict_chat_member(chat_id=message.chat.id,
                                            user_id=message.reply_to_message.from_user.id, can_send_messages=True,
                                            can_send_media_messages=(
                                                    not group.ban_audio or not group.ban_video or not group.ban_file or
                                                    not group.ban_photo),
                                            can_send_other_messages=(not group.ban_gif or not group.ban_sticker),
                                            can_add_web_page_previews=not group.ban_link,
                                            can_invite_users=not group.ban_invite,
                                            can_send_polls=not group.ban_poll)
                message.reply("user {} unmuted.".format(message.reply_to_message.from_user.first_name))

            elif message.text == "!kick":
                try:
                    client.kick_chat_member(chat_id=message.chat.id, user_id=message.reply_to_message.from_user.id)
                except Exception as e:
                    print(e)
            elif message.text == "!warn":
                pass
            elif message.text == "!auto lock":
                if me.status == "administrator":
                    message.reply("auto lock enabled for this group.")
                group.auto_lock = True
                group.save()
                pass
            elif "setautolock" in message.command:
                auto_lock_time = group.auto_lock_time
                btn = InlineKeyboardMarkup(
                    [[InlineKeyboardButton("-",
                                           callback_data=f"lock_minus_hour_{auto_lock_time.hour}_{auto_lock_time.minute}"),
                      InlineKeyboardButton("hour: {} 🕐".format(auto_lock_time.hour),
                                           callback_data="lock_hour"),
                      InlineKeyboardButton("+",
                                           callback_data=f"lock_plus_hour_{auto_lock_time.hour}_{auto_lock_time.minute}")],
                     [
                         InlineKeyboardButton("-",
                                              callback_data=f"lock_minus_minute_{auto_lock_time.hour}_{auto_lock_time.minute}"),
                         InlineKeyboardButton("minute: {} 🕐".format(auto_lock_time.minute),
                                              callback_data="lock_minute"),
                         InlineKeyboardButton("+",
                                              callback_data=f"lock_plus_minute_{auto_lock_time.hour}_{auto_lock_time.minute}")
                     ], [
                         InlineKeyboardButton("Set ✅",
                                              callback_data=f"lock_set_{auto_lock_time.hour}_{auto_lock_time.minute}")
                     ]])

                message.reply("use +/- buttons to edit the hour/minute values and finally press **Set** button.",
                              reply_markup=btn)
            elif "setautounlock" in message.command:
                auto_unlock_time = group.auto_unlock_time
                btn = InlineKeyboardMarkup([[InlineKeyboardButton("-", callback_data="unlock_minus_hour"),
                                             InlineKeyboardButton("hour: {} 🕐".format(auto_unlock_time.hour),
                                                                  callback_data="unlock_hour"),
                                             InlineKeyboardButton("+", callback_data="unlock_plus_hour")],
                                            [
                                                InlineKeyboardButton("-", callback_data="unlock_minus_minute"),
                                                InlineKeyboardButton("minute: {} 🕐".format(auto_unlock_time.minute),
                                                                     callback_data="unlock_minute"),
                                                InlineKeyboardButton("+", callback_data="unlock_plus_minute")
                                            ], [
                                                InlineKeyboardButton("Set ✅", callback_data="unlock_set")
                                            ]])

                message.reply("use +/- buttons to edit the hour/minute values and finaly press **Set** button.",
                              reply_markup=btn)
