import time
from datetime import datetime
import utils
from vars import Vars
import logging
from pyrogram import Client, Filters, Message, InlineKeyboardMarkup, InlineKeyboardButton


@Client.on_message(Filters.group, group=2)
def spam_watcher(client: Client, m: Message):
    start = datetime.now()
    if f'group_{m.chat.id}' not in Vars.GROUP_ADMINS:
        Vars.GROUP_ADMINS[f'group_{m.chat.id}'] = utils.get_chat_admins(client, m)
    end = datetime.now()
    ms = (end - start).microseconds / 1000
    print(f"request response time: {ms}")
    print("warns: ", Vars.WARNS.get(f'group_{m.chat.id}', {}).get(f'user_{m.from_user.id}', 0))
    print("repetitive msgs: ",
          Vars.SPAM_LAST_MESSAGE_USER.get(f'group_{m.chat.id}', {}).get(f'user_{m.from_user.id}', 0) + 1)
    if m.from_user.id not in Vars.GROUP_ADMINS[f'group_{m.chat.id}']:  # check if user is not admin
        if f'group_{m.chat.id}' in Vars.SPAM_LAST_MESSAGE_USER:  # check if this group exists in list
            if f'user_{m.from_user.id}' in Vars.SPAM_LAST_MESSAGE_USER[
                f'group_{m.chat.id}']:
                # check if this user has sent the previous message
                Vars.SPAM_LAST_MESSAGE_USER[f'group_{m.chat.id}'][f'user_{m.from_user.id}'] += 1
                if Vars.SPAM_LAST_MESSAGE_USER[f'group_{m.chat.id}'][f'user_{m.from_user.id}'] > 3:
                    utils.warn(client, m, "spam")
            else:
                Vars.SPAM_LAST_MESSAGE_USER[f'group_{m.chat.id}'] = {}
                Vars.SPAM_LAST_MESSAGE_USER[f'group_{m.chat.id}'][f'user_{m.from_user.id}'] = 1
        else:
            Vars.SPAM_LAST_MESSAGE_USER[f'group_{m.chat.id}'] = {}
            Vars.WARNS[f'group_{m.chat.id}'] = {}
