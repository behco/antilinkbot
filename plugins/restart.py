from pyrogram import Client, Filters
from threading import Thread


def restart(c, m):
    c.restart()
    m.reply_text("restarted!")


@Client.on_message(Filters.command("restart", ["!", "."]) & Filters.user(["103023249", "799644429"]))
def restart_handler(c, m):
    Thread(target=restart, args=(c, m)).start()
