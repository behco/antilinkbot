import datetime

from pyrogram import Client, Filters, Message
from database import Chat


@Client.on_message(Filters.new_chat_members & Filters.group, group=1)
def my_join_handler(client: Client, message: Message):
    print("joined chat")
    group = Chat.get_without_failing((Chat.id == message.chat.id))
    for new_member in message.new_chat_members:
        if new_member.is_self:  # it is me added to new group
            if group is None:  # first time that bot was added to the group
                try:
                    Chat.create(id=message.chat.id, title=message.chat.title, username=message.from_user.username,
                                chat_id=message.chat.id)
                except Exception as e:
                    print(e)
            else:  # bot has already been in this group. update the some statue
                Chat.update({Chat.is_bot_in: False, Chat.updated_at: datetime.datetime.now()}).where(
                    (Chat.id == message.chat.id)).execute()

        elif new_member.is_bot:
            if group is not None:
                if group.ban_bot:
                    try:
                        pass
                        # client.kick_chat_member(chat_id=message.chat.id, user_id=new_member.id, until_date=60)
                    except Exception as e:
                        print("error kicking bot: ", e)
    print("done new group")