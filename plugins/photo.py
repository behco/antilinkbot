from pyrogram import Client, Filters, Message
from database import Chat


@Client.on_message(Filters.photo & Filters.group,group=1)
def my_photo_handler(client: Client, message: Message):
    print(message)
    group = Chat.get_without_failing((Chat.id == message.chat.id))
    print("photo detected")
    if group is not None:
        if group.ban_photo:
            member = client.get_chat_member(chat_id=group.id, user_id=message.from_user.id)
            if member.status in ['creator', 'administrator']:
                pass
            else:
                try:
                    message.delete(revoke=True)
                except Exception as e:
                    print(e)

