import datetime

from pyrogram import Client, Filters, Message
from database import Chat


@Client.on_message(Filters.left_chat_member & Filters.group, group=1)
def my_voice_handler(client: Client, message: Message):
    print("left chat")
    if message.left_chat_member.is_self:
        print("bot deleted from the group.")
        Chat.update({Chat.is_bot_in: False, Chat.updated_at: datetime.datetime.now()}).where(
            (Chat.id == message.chat.id)).execute()
