import datetime

from pyrogram import Client, Filters, Message, CallbackQuery, InlineKeyboardMarkup, InlineKeyboardButton
from database import Chat


@Client.on_callback_query()
def my_callback_query_handler(client: Client, message: CallbackQuery):
    data = message.data
    group = Chat.get_without_failing((Chat.id == message.message.chat.id))
    if data.startswith("lock"):
        data = data.split("_")
        if {"minus", "hour"}.issubset(set(data)):
            hour = int(data[-2])
            minute = int(data[-1])
            if hour == 0:
                hour = 23
            else:
                hour -= 1
            btn = InlineKeyboardMarkup(
                [[InlineKeyboardButton("-", callback_data=f"lock_minus_hour_{hour}_{minute}"),
                  InlineKeyboardButton("hour: {} 🕐".format(hour),
                                       callback_data="lock_hour"),
                  InlineKeyboardButton("+", callback_data=f"lock_plus_hour_{hour}_{minute}")],
                 [
                     InlineKeyboardButton("-", callback_data=f"lock_minus_minute_{hour}_{minute}"),
                     InlineKeyboardButton("minute: {} 🕐".format(minute),
                                          callback_data="lock_minute"),
                     InlineKeyboardButton("+", callback_data=f"lock_plus_minute_{hour}_{minute}")
                 ], [
                     InlineKeyboardButton("Set ✅",
                                          callback_data=f"lock_set_{hour}_{minute}")
                 ]])
            message.edit_message_reply_markup(reply_markup=btn)
        elif {"plus", "hour"}.issubset(set(data)):
            hour = int(data[-2])
            minute = int(data[-1])
            if hour == 23:
                hour = 0
            else:
                hour += 1
            btn = InlineKeyboardMarkup(
                [[InlineKeyboardButton("-", callback_data=f"lock_minus_hour_{hour}_{minute}"),
                  InlineKeyboardButton("hour: {} 🕐".format(hour),
                                       callback_data="lock_hour"),
                  InlineKeyboardButton("+", callback_data=f"lock_plus_hour_{hour}_{minute}")],
                 [
                     InlineKeyboardButton("-", callback_data=f"lock_minus_minute_{hour}_{minute}"),
                     InlineKeyboardButton("minute: {} 🕐".format(minute),
                                          callback_data="lock_minute"),
                     InlineKeyboardButton("+", callback_data=f"lock_plus_minute_{hour}_{minute}")
                 ], [
                     InlineKeyboardButton("Set ✅",
                                          callback_data=f"lock_set_{hour}_{minute}")
                 ]])
            message.edit_message_reply_markup(reply_markup=btn)
        elif {"minus", "minute"}.issubset(set(data)):
            hour = int(data[-2])
            minute = int(data[-1])
            if minute == 0:
                minute = 55
            else:
                minute -= 5
            btn = InlineKeyboardMarkup(
                [[InlineKeyboardButton("-", callback_data=f"lock_minus_hour_{hour}_{minute}"),
                  InlineKeyboardButton("hour: {} 🕐".format(hour),
                                       callback_data="lock_hour"),
                  InlineKeyboardButton("+", callback_data=f"lock_plus_hour_{hour}_{minute}")],
                 [
                     InlineKeyboardButton("-", callback_data=f"lock_minus_minute_{hour}_{minute}"),
                     InlineKeyboardButton("minute: {} 🕐".format(minute),
                                          callback_data="lock_minute"),
                     InlineKeyboardButton("+", callback_data=f"lock_plus_minute_{hour}_{minute}")
                 ], [
                     InlineKeyboardButton("Set ✅",
                                          callback_data=f"lock_set_{hour}_{minute}")
                 ]])
            message.edit_message_reply_markup(reply_markup=btn)
        elif {"plus", "minute"}.issubset(set(data)):
            hour = int(data[-2])
            minute = int(data[-1])
            if minute == 55:
                minute = 0
            else:
                minute += 5
            btn = InlineKeyboardMarkup(
                [[InlineKeyboardButton("-", callback_data=f"lock_minus_hour_{hour}_{minute}"),
                  InlineKeyboardButton("hour: {} 🕐".format(hour),
                                       callback_data="lock_hour"),
                  InlineKeyboardButton("+", callback_data=f"lock_plus_hour_{hour}_{minute}")],
                 [
                     InlineKeyboardButton("-", callback_data=f"lock_minus_minute_{hour}_{minute}"),
                     InlineKeyboardButton("minute: {} 🕐".format(minute),
                                          callback_data="lock_minute"),
                     InlineKeyboardButton("+", callback_data=f"lock_plus_minute_{hour}_{minute}")
                 ], [
                     InlineKeyboardButton("Set ✅",
                                          callback_data=f"lock_set_{hour}_{minute}")
                 ]])
            message.edit_message_reply_markup(reply_markup=btn)
        elif {"plus", "minute"}.issubset(set(data)):
            pass
        elif "hour" in data:
            pass
        elif "minute" in data:
            pass
        elif "set" in data:
            hour = int(data[-2])
            minute = int(data[-1])
            lock_time = datetime.datetime.now().time()
            lock_time = lock_time.replace(hour=int(hour), minute=int(minute))
            print("lock time:", lock_time)
            group.auto_lock_time = lock_time
            group.save()
