import datetime

from pyrogram import Client
from config import Config
from database import Chat

bot = Client(Config.SESSION_NAME, bot_token=Config.BOT_TOKEN,
             api_id=Config.API_ID,
             api_hash=Config.API_HASH)


def auto_lock_group():
    now = datetime.datetime.now().time()
    for group in Chat.select():
        auto_lock_time = group.auto_lock_time
        auto_unlock_time = group.auto_unlock_time
        if group.auto_lock:
            if auto_lock_time.hour == now.hour and auto_lock_time.minute == now.minute:
                print("auto lock is on for this group")
                bot.send_message(chat_id=group.id, text="group locked")
                group.ban_photo = True
                group.ban_link = True
                group.ban_file = True
                group.ban_gif = True
                group.ban_sticker = True
                group.ban_audio = True
                group.ban_video_note = True
                group.ban_video = True
                group.ban_voice = True
                group.ban_poll = True
                group.save()
            elif auto_unlock_time.hour == now.hour and auto_unlock_time.minute == now.minute:
                bot.send_message(chat_id=group.id, text="group unlocked")
                group.ban_photo = False
                group.ban_link = False
                group.ban_file = False
                group.ban_gif = False
                group.ban_sticker = False
                group.ban_audio = False
                group.ban_video_note = False
                group.ban_video = False
                group.ban_voice = False
                group.ban_poll = False
                group.save()


def auto_unlock_group():
    for group in Chat.select():
        if group.auto_lock:
            bot.send_message(chat_id=group.id, text="group unlocked")
            group.ban_photo = False
            group.ban_link = False
            group.ban_file = False
            group.ban_gif = False
            group.ban_sticker = False
            group.ban_audio = False
            group.ban_video_note = False
            group.ban_video = False
            group.ban_voice = False
            group.save()


# schedule.every().minute.do(auto_lock_group)
# schedule.every().day.at('01:06').do(auto_unlock_group)

bot.run()
# while True:
#     schedule.run_pending()
#     time.sleep(1)
