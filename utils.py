import time

from pyrogram import Client, Message, InlineKeyboardMarkup, InlineKeyboardButton

from vars import Vars


def get_chat_admins(client: Client, message) -> list:
    admins = client.get_chat_members(message.chat.id, filter="administrators")
    admin_ids = (admin.user.id for admin in admins)
    return list(admin_ids)


def warn(client: Client, m: Message, warn_type):
    if f'user_{m.from_user.id}' in Vars.WARNS[f'group_{m.chat.id}']:
        Vars.WARNS[f'group_{m.chat.id}'][f'user_{m.from_user.id}'] += 1
        if Vars.WARNS[f'group_{m.chat.id}'][f'user_{m.from_user.id}'] >= Vars.MAX_WARNS_UNTIL_BAN:
            # ban user from group
            Vars.MUTE_UNIX_TIME[f'user_{m.from_user.id}'] = time.time() + 5
            Vars.WARNS[f'group_{m.chat.id}'][f'user_{m.from_user.id}'] = 0
            try:
                del Vars.SPAM_LAST_MESSAGE_USER[f'group_{m.chat.id}'][f'user_{m.from_user.id}']
            except KeyError as e:
                print(e)
            try:
                del Vars.FLOOD_LAST_MESSAGE_USER[f'group_{m.chat.id}'][f'user_{m.from_user.id}']
            except KeyError as e:
                print(e)
            m.reply(Vars.BANNED['warn'].format(m.from_user.id, m.from_user.id))
            client.kick_chat_member(m.chat.id, m.from_user.id, Vars.BAN_TIME)
        elif Vars.WARNS[f'group_{m.chat.id}'][
            f'user_{m.from_user.id}'] >= Vars.MAX_WARNS_UNTIL_MUTE_3:
            # mute for 3rd time
            if Vars.WARNS[f'group_{m.chat.id}'][
                f'user_{m.from_user.id}'] == Vars.MAX_WARNS_UNTIL_MUTE_3:
                if Vars.MUTE_UNIX_TIME.get(f'user_{m.from_user.id}', int(time.time())) <= int(
                        time.time()):
                    # it is first time
                    if warn_type == "spam":
                        Vars.SPAM_LAST_MESSAGE_USER[f'group_{m.chat.id}'] = {}
                    elif warn_type == "flood":
                        del Vars.FLOOD_LAST_MESSAGE_USER[f'group_{m.chat.id}'][f'user_{m.from_user.id}']
                    Vars.MUTE_UNIX_TIME[f'user_{m.from_user.id}'] = time.time() + Vars.MUTE_TIME_3
                    client.restrict_chat_member(chat_id=m.chat.id,
                                                user_id=m.from_user.id,
                                                until_date=int(time.time()) + Vars.MUTE_TIME_3)
                    btn_unmute = InlineKeyboardMarkup([
                        [InlineKeyboardButton("✅ Unmute", callback_data="unmute {}".format(
                            m.from_user.id))]])
                    m.reply(Vars.MUTED['warn'].format(m.from_user.first_name, m.from_user.id,
                                                      pretty_time_delta(Vars.MUTE_TIME_3)
                                                      ),
                            reply_markup=btn_unmute)
            else:
                try:
                    del Vars.MUTE_UNIX_TIME[f'user_{m.from_user.id}']
                except KeyError as e:
                    print("doesn't exist:", e)
                Vars.SPAM_LAST_MESSAGE_USER[f'group_{m.chat.id}'][f'user_{m.from_user.id}'] = 1
                m.reply(
                    Vars.WARN[warn_type].format(Vars.WARNS[f'group_{m.chat.id}'][f'user_{m.from_user.id}'],
                                                Vars.MAX_WARNS_UNTIL_BAN))
        elif Vars.WARNS[f'group_{m.chat.id}'][
            f'user_{m.from_user.id}'] >= Vars.MAX_WARNS_UNTIL_MUTE_2:
            # mute for 2nd time
            if Vars.WARNS[f'group_{m.chat.id}'][
                f'user_{m.from_user.id}'] == Vars.MAX_WARNS_UNTIL_MUTE_2:
                if Vars.MUTE_UNIX_TIME.get(f'user_{m.from_user.id}', int(time.time())) <= int(
                        time.time()):
                    # it is first time
                    if warn_type == "spam":
                        Vars.SPAM_LAST_MESSAGE_USER[f'group_{m.chat.id}'] = {}
                    elif warn_type == "flood":
                        Vars.FLOOD_LAST_MESSAGE_USER[f'group_{m.chat.id}'][f'user_{m.from_user.id}'] = 1
                    Vars.MUTE_UNIX_TIME[f'user_{m.from_user.id}'] = time.time() + Vars.MUTE_TIME_2
                    client.restrict_chat_member(chat_id=m.chat.id,
                                                user_id=m.from_user.id,
                                                until_date=int(time.time()) + Vars.MUTE_TIME_2)
                    btn_unmute = InlineKeyboardMarkup([
                        [InlineKeyboardButton("✅ Unmute", callback_data="unmute {}".format(
                            m.from_user.id))]])
                    m.reply(Vars.MUTED['warn'].format(m.from_user.first_name, m.from_user.id,
                                                      pretty_time_delta(Vars.MUTE_TIME_2)),
                            reply_markup=btn_unmute)
            else:
                del Vars.MUTE_UNIX_TIME[f'user_{m.from_user.id}']
                Vars.SPAM_LAST_MESSAGE_USER[f'group_{m.chat.id}'][f'user_{m.from_user.id}'] = 1
                m.reply(
                    Vars.WARN[warn_type].format(Vars.WARNS[f'group_{m.chat.id}'][f'user_{m.from_user.id}'],
                                                Vars.MAX_WARNS_UNTIL_MUTE_3))
        elif Vars.WARNS[f'group_{m.chat.id}'][
            f'user_{m.from_user.id}'] >= Vars.MAX_WARNS_UNTIL_MUTE_1:
            if Vars.WARNS[f'group_{m.chat.id}'][
                f'user_{m.from_user.id}'] == Vars.MAX_WARNS_UNTIL_MUTE_1:  # warn ==4, mute him
                if Vars.MUTE_UNIX_TIME.get(f'user_{m.from_user.id}', int(time.time())) <= int(
                        time.time()):
                    # it is first time
                    print("inside first time block")

                    if warn_type == "spam":
                        Vars.SPAM_LAST_MESSAGE_USER[f'group_{m.chat.id}'] = {}
                    elif warn_type == "flood":
                        Vars.FLOOD_LAST_MESSAGE_USER[f'group_{m.chat.id}'][f'user_{m.from_user.id}'] = 1
                    Vars.MUTE_UNIX_TIME[f'user_{m.from_user.id}'] = time.time() + Vars.MUTE_TIME_1
                    client.restrict_chat_member(chat_id=m.chat.id,
                                                user_id=m.from_user.id,
                                                until_date=int(time.time()) + Vars.MUTE_TIME_1)
                    btn_unmute = InlineKeyboardMarkup([
                        [InlineKeyboardButton("✅ Unmute", callback_data="unmute {}".format(
                            m.from_user.id))]])
                    m.reply(Vars.MUTED['warn'].format(m.from_user.first_name, m.from_user.id,
                                                      pretty_time_delta(Vars.MUTE_TIME_1)
                                                      ),
                            reply_markup=btn_unmute)

            else:  # warn == 5 , warn her
                if warn_type == "spam":
                    Vars.SPAM_LAST_MESSAGE_USER[f'group_{m.chat.id}'] = {}
                elif warn_type == "flood":
                    Vars.FLOOD_LAST_MESSAGE_USER[f'group_{m.chat.id}'][f'user_{m.from_user.id}'] = 1
                m.reply(
                    Vars.WARN[warn_type].format(Vars.WARNS[f'group_{m.chat.id}'][f'user_{m.from_user.id}'],
                                                Vars.MAX_WARNS_UNTIL_MUTE_2))

        else:  # less than 3 warns
            if warn_type == "spam":
                Vars.SPAM_LAST_MESSAGE_USER[f'group_{m.chat.id}'] = {}
            elif warn_type == "flood":
                Vars.FLOOD_LAST_MESSAGE_USER[f'group_{m.chat.id}'][f'user_{m.from_user.id}'] = 1
            m.reply(
                Vars.WARN[warn_type].format(Vars.WARNS[f'group_{m.chat.id}'][f'user_{m.from_user.id}'],
                                            Vars.MAX_WARNS_UNTIL_MUTE_1))
    else:
        Vars.WARNS[f'group_{m.chat.id}'][f'user_{m.from_user.id}'] = 1
        if warn_type == "spam":
            Vars.SPAM_LAST_MESSAGE_USER[f'group_{m.chat.id}'] = {}
        elif warn_type == "flood":
            Vars.FLOOD_LAST_MESSAGE_USER[f'group_{m.chat.id}'][f'user_{m.from_user.id}'] = 1
        m.reply(Vars.WARN[warn_type].format(Vars.WARNS[f'group_{m.chat.id}'][f'user_{m.from_user.id}'],
                                            Vars.MAX_WARNS_UNTIL_MUTE_1)
                )


def pretty_time_delta(seconds):
    seconds = int(seconds)
    days, seconds = divmod(seconds, 86400)
    hours, seconds = divmod(seconds, 3600)
    minutes, seconds = divmod(seconds, 60)
    if days > 0:
        if hours > 0:
            return '%d day{} and %d hour{}'.format('s' if days > 1 else
                                                   '', 's' if hours > 1 else
                                                   '') % (days, hours)
        else:
            return '%d day{}'.format('s' if days > 1 else
                                     '') % days
    elif hours > 0:
        if minutes > 0:
            return '%d hour{} and %d minute{}'.format('s' if hours > 1 else
                                                      '', 's' if minutes > 1 else
                                                      '') % (hours, minutes)
        else:
            return '%d hour{}'.format('s' if hours > 1 else
                                      '') % hours
    elif minutes > 0:
        return '%d minute{}'.format('s' if minutes > 1 else
                                    '') % minutes
    else:
        return '%d second{}'.format('s' if seconds > 1 else
                                    '') % seconds
