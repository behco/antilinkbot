from peewee import *
import logging
import datetime
from peewee import BooleanField
from dateutil.relativedelta import *

db = MySQLDatabase('AntiLink', user='root', password='',
                   host='localhost', port=3306)
logger = logging.getLogger('peewee')
logger.addHandler(logging.StreamHandler())
logger.setLevel(logging.DEBUG)


class BaseModel(Model):
    class Meta:
        database = db


class User(BaseModel):
    id = BigIntegerField(primary_key=True)
    chat_id = BigIntegerField(null=True)
    username = TextField(unique=True, null=True, default=None, index=True)
    first_name = TextField(null=True, help_text="User's first name", default=None)
    last_name = TextField(null=True, help_text="User's last name", default=None)
    is_admin = BooleanField(default=False, help_text="is this user an admin of this bot?")
    status = CharField(max_length=6, default="free")
    paid_start = DateTimeField(null=True)
    paid_end = DateTimeField(null=True)
    invite_code = TextField(null=True)
    invited_by = ForeignKeyField('self', null=True)
    language_code = CharField(max_length=5, null=True)
    created_at = DateTimeField(default=datetime.datetime.now)
    updated_at = DateTimeField(default=datetime.datetime.now)

    @staticmethod
    def get_without_failing(query):
        results = User.select().where(query).limit(1)
        return results[0] if len(results) > 0 else None


class Chat(BaseModel):
    id = BigIntegerField(primary_key=True, help_text='Unique user or chat identifier')
    type = SmallIntegerField(null=True, help_text='Chat type, either private, group, supergroup or channel')
    title = TextField(null=True, help_text='Chat (group) title, is null if chat type is private')
    is_bot_in = BooleanField(default=True, help_text="if bot is left/removed or not")
    all_members_are_administrators = BooleanField(default=False,
                                                  help_text='True if a all members of this group are admins')
    auto_lock = BooleanField(default=False, help_text="should automatically lock the group")
    ban_voice = BooleanField(default=False, help_text="should ban voice messages")
    ban_audio = BooleanField(default=False, help_text="should ban audio messages")
    ban_link = BooleanField(default=False, help_text="should ban link messages")
    ban_file = BooleanField(default=False, help_text="should ban file messages")
    ban_video_note = BooleanField(default=False, help_text="should ban video_note messages")
    ban_photo = BooleanField(default=False, help_text="should ban photo messages")
    ban_video = BooleanField(default=False, help_text="should ban video messages")
    ban_gif = BooleanField(default=False, help_text="should ban gif messages")
    ban_sticker = BooleanField(default=False, help_text="should ban sticker messages")
    ban_text = BooleanField(default=False, help_text="should ban text messages")
    ban_bot = BooleanField(default=False, help_text="should ban bots")
    ban_poll = BooleanField(default=False, help_text="should ban polls")
    ban_invite = BooleanField(default=False, help_text="should ban invites")
    free_end_at = DateTimeField(default=datetime.datetime.now() + relativedelta(days=+2))
    auto_lock_time = TimeField(help_text="when auto lock group", formats='%H:%M', default=datetime.time(23, 30))
    auto_unlock_time = TimeField(help_text="when auto lock group", formats='%H:%M', default=datetime.time(7, 30))
    created_at = DateTimeField(default=datetime.datetime.now)
    updated_at = DateTimeField(default=datetime.datetime.now)

    old_id = BigIntegerField(null=True,
                             help_text='Unique chat identifier, this is filled when a group is converted to a supergroup')

    @staticmethod
    def get_without_failing(query):
        results = Chat.select().where(query).limit(1)
        return results[0] if len(results) > 0 else None


class UserChat(BaseModel):
    user = ForeignKeyField(User, help_text='Unique user identifier')
    chat = ForeignKeyField(Chat, help_text='Unique user or chat identifier')

    class Meta:
        primary_key = CompositeKey('user', 'chat')


db.connect()
db.create_tables([User, Chat, UserChat])
