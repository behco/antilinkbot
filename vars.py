class Vars(object):
    # list of admins of every group,useful for prevent from sending requests to telegram on every message received
    GROUP_ADMINS = {}
    # count of warns a user has received because of spamming, contains all users that have received a warning
    WARNS = {}

    # count of messages from same user, contains only one key for every group
    SPAM_LAST_MESSAGE_USER = {}

    # "GROUP_1234": {'user_456': 4, "user_789": 3}
    FLOOD_LAST_MESSAGE_USER = {}

    MUTE_UNIX_TIME = {}  # this will be useful for avoiding sending multiple "user has been banned" messages

    MAX_WARNS_UNTIL_MUTE_1 = 3
    MAX_WARNS_UNTIL_MUTE_2 = 5
    MAX_WARNS_UNTIL_MUTE_3 = 7
    MAX_WARNS_UNTIL_BAN = 10

    MUTE_TIME_1 = 60 * 60 * 10  # mute for first time, 10 hours
    MUTE_TIME_2 = 60 * 60 * 24  # mute for second time, 1 day
    MUTE_TIME_3 = 60 * 60 * 24 * 3  # mute for third time, 3 days

    FLOOD_MUTE_TIME = 30  # if any flooding detected, user will be restrict for this time to prevent from flooding

    BAN_TIME = 60 * 60 * 24 * 10  # ban for 10 days

    MAX_MSGS_UNTIL_SPAM_WARN = 3  # maximum msgs that will trigger spam warn
    MAX_MSGS_UNTIL_FLOOD_WARN = 4  # maximum msgs that will trigger flood warn
    FLOOD_TIME_LIMIT = 1.5  # in

    WARN = {"spam": "⚠ **Spam Warn {}/{}**\n\nPlease avoid spreading your messages into several messages.",
            "flood": "⚠ **Flood warn {}/{}**\n\nPlease avoid sending messages too quickly."}
    BANNED = {"warn": "User [{}](tg://user?id={}) has been banned 🚫 from this group because of reaching Max warnings."}
    MUTED = {"warn": "User [{}](tg://user?id={}) has been muted 🔇 for {} because of reaching maximum warns."}
